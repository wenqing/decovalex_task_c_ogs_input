# 1. open the vtu mesh file with ParaView
# 2. use programmable filter of Paraview
# 3. copy the script to its window
# 4. apply it
# change the computation in the script to get better distribution

# Get the data
A = self.GetInputDataObject(0, 0)
numPoints = A.GetNumberOfPoints()

outp = vtk.vtkDoubleArray()
outp.SetName('p0')

for i in range(0, numPoints):
                                                # Domain
      outp.InsertNextValue(2.0e+6)
                                                # Clay
for i in range(0, numPoints):
      coord = A.GetPoint(i)
      x, y, z = coord[:3]
      r=sqrt(x*x+y*y)
      if r<=1.25:                              # Tunnel+Pedestral
          if (x>=-0.41 and x<=0.41 and y<0):     # Pedestral
              if (x>-0.39 and x<0.39 and y<0): # Pedestral Inner x
                  if r<1.23:                   # Pedestral Inner x+r
                      outp.SetValue(i, -11980877.6101033 )
                                               # Block
                  else:                        # Domain/Pedestral
                      outp.SetValue(i,  -4990438.80505165)
                                               # Clay  /Block
              else:                            # Tunnel/Pedestral
                  if r<1.23:                   # Tunnel/Pedestral Inner r
                      outp.SetValue(i, -67071994.1334582 )
                                               # Bent  /Block
                  else:                        # Domain/Tunnel/Pedestral
                      outp.SetValue(i, -44047996.0889722 )
                                               # Clay  /Bent  /Block
          else:                                # Tunnel
              if r<1.23:                       # Tunnel Inner r
                  outp.SetValue(i,-122163110.6568132 )
                                               # Bent
              else:                            # Domain/Tunnel
                  outp.SetValue(i, -60081555.3284066 )
                                               # Clay  /Bent

output = self.GetOutput()
output.GetPointData().AddArray(outp)

numCells = A.GetNumberOfCells()
MatIDs = A.GetCellData().GetScalars('MaterialIDs')

output.GetCellData().AddArray(MatIDs)
